from setuptools import setup, find_packages

setup(name='sna',
      version='0.1',
      description='Analisis de Redes Sociales, de Sentimiento y Sistemas de Recomendacion para estrategias comerciales',
      author='Marcelo Tuller',
      author_email='marscrophimself@protonmail.com',
      license='GPL3',
      packages=find_packages(),
      install_requires=[
          'numpy', 'networkx', 'keras', 'wxPython','nltk', 'twython', 'tweepy', 'matplotlib', 'bs4', 'scikit-learn'
      ],
      zip_safe=False)
