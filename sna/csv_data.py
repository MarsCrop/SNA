import csv as CSV

def csv_save(filename, array): 
    """Guardar datos de una lista en un archivo .csv
    -param filename: nombre de archivo a guardar
    -param array: lista para guardar en csv"""           
    with open(filename, 'w') as f:         
        for i in array:          
            CSV.writer(f).writerow(i)

def textos_save(filename, array): 
    """Guardar textos obtenidos en un archivo .csv
    -param filename: nombre de archivo a guardar
    -param array: lista para guardar en csv"""          
    with open(filename, 'w') as f:         
        CSV.writer(f).writerow(array)

def textos_twitter_save(filename, array): 
    """Guardar lista de tweets en un archivo .csv
    -param filename: nombre de archivo a guardar
    -param array: lista para guardar en csv"""     
    with open(filename, 'w') as f:
        for i in array:              
            try:                                      
                CSV.writer(f).writerow(i)
            except Exception as e:                                      
                continue

def csv_read(filename, data_type):
    """Leer datos de una lista en un archivo .csv
    -param filename: nombre de archivo a guardar
    -param data_type: especificar si quiere leer los datos del archivo .csv como lista o tupla"""         
    with open(filename, 'r') as f:         
        reader = CSV.reader(f)
        opened = data_type(reader)
    return opened

def SaveNGrams(filename, array):
    """Guardar bigramas, trigramas o ngramas en un archivo .csv
    -param filename: nombre de archivo a guardar
    -param array: lista para guardar en csv"""                                        
    with open(filename, 'w') as f:
        for i in array:                                     
            for j in i:                                      
                CSV.writer(f).writerow(list(j))

def write_sentiments_csv(mt, p, filename):
    """Guardar sentimientos en un archivo .csv
    -param mt: lista de textos utilizados para el analisis de sentimiento
    -param p: lista de intensidades utilizadas para el analisis de sentimiento
    -param filename: nombre de archivo para guardar en .csv"""         
    with open(filename, 'w') as f:
        for i in range(len(mt)):
            try:  
                if p[i] == 0:
                    CSV.writer(f).writerow(list([mt[i],'neg']))
                if p[i] == 1:                                  
                    CSV.writer(f).writerow(list([mt[i],'neu']))
                if p[i] == 2:                              
                    CSV.writer(f).writerow(list([mt[i],'pos']))
            except UnicodeEncodeError:                     
                continue 

def write_recomendaciones_csv(recommend, filename):
    """Guardar recomendaciones en un archivo .csv
    -param recommend: lista de recomendados
    -param filename: nombre de archivo para guardar en .csv""" 
    with open(filename, 'w') as f:
        CSV.writer(f).writerow(['Hashtag/Producto', 'precision Top', 'Apoyo'])
        for i in range(len(recommend)):
            try:
                csv.writer(f).writerow(recommend[i])
            except Exception:
                continue

def write_palabras_csv(palabras, filename):
    """Guardar palabras mas utilizadas en un archivo .csv
    -param palabras: lista de palabras mas utilizadas
    -param filename: nombre de archivo para guardar en .csv""" 
    with open(filename, 'w') as f:
        CSV.writer(f).writerow(['Palabra', 'Cantidad de Repeticiones'])
        for i in range(len(palabras)):
            try:
                CSV.writer(f).writerow(palabras[i])
            except Exception:
                continue



