import sys, os
from login import *
import numpy as np
from random import randint
from sklearn.preprocessing import MinMaxScaler
from sklearn.utils import compute_sample_weight, compute_class_weight
from sklearn import cross_validation, metrics
from sklearn.feature_extraction.text import CountVectorizer
import tweepy
from nlp import *
from nltk.sentiment.util import mark_negation
from nltk.sentiment import SentimentIntensityAnalyzer as sid
from operator import itemgetter
import random
import warnings

os.environ['KERAS_BACKEND'] = 'theano'

from keras.models import Sequential
from keras.layers import Dense
from keras.layers import Flatten
from keras.layers.convolutional import Conv1D
from keras.layers.convolutional import MaxPooling1D
from keras.layers.embeddings import Embedding
from keras.preprocessing import sequence
from keras.regularizers import l2
from keras.utils import to_categorical
from keras.constraints import non_neg
from keras.constraints import min_max_norm

def fxn():
    warnings.warn("deprecated", DeprecationWarning)

with warnings.catch_warnings():
    warnings.simplefilter("ignore")
    fxn()

count = CountVectorizer(analyzer="word",                   
                                   tokenizer=lambda text: mark_negation(process_vector(text)),                                                    
                                   max_features=10000)

count_i = CountVectorizer()

api = login()

def find_likes(username, n, session):
    """Buscar paginas que le gustan a usuario x
    -param username: nombre de archivo a guardar
    -param n: cantidad de likes que desea obtener del usuario (hasta 30)"""                                   
    session.get('https://m.facebook.com/' + username)      
                                                                      
    soup = BeautifulSoup(session.page_source, "lxml") 

    try:
        for i in soup.findAll('a', href = True):                      
            if username+'?v=likes&amp' in str(i):
                likes = (i['href'])
        session.get('https://m.facebook.com/' + likes) 
    except UnboundLocalError:
        print (username + " no permite leer sus likes")
        return []     
                                                                      
    soup = BeautifulSoup(session.page_source) 

    likes = []                                                        
    for links in soup.findAll('a', href = True):                      
        if 'span' in str(links) and not 'profile' in str(links) and not 'class' in str(links) and not 'sectionid' in str(links):            
            likes.append(links) 
    for i in range(len(likes)):                                       
        likes[i] = str(likes[i]).split('/')[1]

    pages = likes[:n]  

    random.shuffle(pages)

    return pages

def fetch_posts_pages(pages, session):
    """Obtener posts del perfil de usuario
    -param pages: nombre de archivo a guardar"""                                    
    session.get('https://m.facebook.com/' + pages + '/posts/?ref=page_internal')    

    soup = BeautifulSoup(session.page_source, "lxml")  

    posts = []                                                                      
    for i in soup.findAll('p'):
        posts.append((i.text, pages))

    return posts

def buscar_hashtags(api, hashtags, n):
    """Funcion simple para obtener tweets en base a hashtags
    -param api: sesion de Twitter API
    -param hashtags: lista de hashtags
    -param n: cantidad de tweets por obtener por cada hashtag""" 
    rows = []           
    for hashtag in hashtags:                                           
        for tweet in tweepy.Cursor(api.search,                                                                       
                                   q= hashtag,                             
                                   rpp = 10,                                                              
                                   include_entities = True,                                                           
                                   lang = "es").items(n):             
            text =  ' '.join(tweet.text.replace('\n', ' ').split()).encode('utf-8')                                     
            rows.append([text, str(hashtag).lower()])       
    return rows

def intensidad_de_sentimiento(tokens): 
    """Encuentra la polaridad en una lista de textos. Los resultados pueden ser 'negativo', 'neutral' o 'positivo'
    -param tokens: listas de palabras significativas en los textos""" 
    intensidad = [[] for i in range(len(tokens))]
    for texto in range(len(tokens)):
        for tweet in tokens[texto]:
            print(tweet)
            try:
                ss = sid().polarity_scores(tweet) #ademas devuelve 'compound' (fuerza del sentimiento) que da a conocer la fuerza con la que aparece la polaridad
                for k in sorted(ss):
                    print('{0}: {1}, '.format(k, ss[k]))
                    if np.argmax(list(ss.values())[:3]) == 0:
                        intensidad[texto].append(0)
                    if np.argmax(list(ss.values())[:3]) == 1:
                        intensidad[texto].append(1)
                    if np.argmax(list(ss.values())[:3]) == 2:
                        intensidad[texto].append(2)
            except UnicodeDecodeError:
                tokens[texto].remove(tweet)
                pass
    return intensidad

def intensidad_de_sentimiento_linkedin(tokens):
    """Encuentra la polaridad en una lista de textos de LinkedIn. Los resultados pueden ser 'negativo', 'neutral' o 'positivo'
    -param tokens: listas de palabras significativas en los textos""" 
    intensidad = []
    for texto in tokens:
        print(texto)
        try:
            ss = sid().polarity_scores(texto)
            for k in sorted(ss):
                print('{0}: {1}, '.format(k, ss[k]))
            if np.argmax(list(ss.values())[:3]) == 0:
                intensidad.append(1)
            if np.argmax(list(ss.values())[:3]) == 1:
                intensidad.append(0)
            if np.argmax(list(ss.values())[:3]) == 2:
                intensidad.append(2)
        except UnicodeDecodeError:
            tokens.remove(texto)
            pass
    return intensidad

#antes usabamos solamente los textos, ahora queremos saber si que podemos recomendar a los usuarios en la red utilizando los hashtags de las influencias para la recomendacion
def generar_rasgos_recomendaciones_desde_usuarios(textos):
    """Tomar solamente aquellos tweets que contengan hashtags, en el caso de que la recomendacion este basada enlas preferencias de usuarios en la red
    -param textos: listas de textos""" 
    influencias_train = []
    for i in range(len(textos)):
        for j in textos[i]:
            tag = ['#'+word[1:] for word in j.split() if word.startswith('#')]
            if len(tag) == 0:
                pass
            else:
                if len(tag) > 1:
                    tag = tag[0]
                influencias_train.append([j,tag])
    return influencias_train

def generar_lista_hashtags(test): 
    """Genera una lista de hashtags que aparecen en tweets
    -param test: listas de textos para evaluar la recomendacion"""      
    hashtags = []
    for i in test:
        if type(i[1]) == list:
            ht = i[1][0]
        else:
            ht = i[1]
        if not ht in hashtags:
            hashtags.append(ht)
    return hashtags

def limpiar_test(test):  
    """Convierte hashtags encapsulados en listas a hashtags de los tweets
    -param test: lista de textos con sus respectivos hashtags"""          
    for i in range(len(test)): 
        if type(test[i][1]) == list:
            test[i][1] = test[i][1][0]
    return test 

def gen_lista(textos):
    """Genera lista de textos compatible con las dimensiones de analisis
    -param textos: textos"""
    t = []
    for i in textos:
        for j in i:
            t.append(j)
    return t

def gen_lista_tokens(tokens):
    """Genera lista de tokens compatible con las dimensiones de analisis
    -param textos: tokens"""
    t = []
    for i in tokens:
        for j in i:
            t.append(j)
    return t

def gen_intens(intensidades):
    """Genera lista de intensidades compatible con las dimensiones de analisis
    -param intensidades: intensidades"""
    intens = []           
    for i in intensidades:
        for j in i:         
            intens.append(j)
    return intens

transform = lambda lista_tweets: count.fit_transform(lista_tweets).toarray()

def CNN_sentimiento(t, intens):
    """Realizar analisis profundo de sentimiento de textos con una lista precomputada de intensidades
    -param t: matriz que representa las veces que aparece una palabra en un texto
    -param intens: intensidades de los textos"""
    intens = np.array(intens)
    t = sequence.pad_sequences(t, maxlen=500)
    t = MinMaxScaler().fit_transform(t)
    x_train = np.vstack((t[intens==0][:int(len(intens[intens==0])/2)],t[intens==1][:int(len(intens[intens==1])/2)],t[intens==2][:int(len(intens[intens==2])/2)]))
    x_test = np.vstack((t[intens==0][int(len(intens[intens==0])/2):],t[intens==1][int(len(intens[intens==1])/2):],t[intens==2][int(len(intens[intens==2])/2):]))
    y_train = np.hstack((intens[intens==0][:int(len(intens[intens==0])/2)],intens[intens==1][:int(len(intens[intens==1])/2)],intens[intens==2][:int(len(intens[intens==2])/2)]))
    y_test = np.hstack((intens[intens==0][int(len(intens[intens==0])/2):],intens[intens==1][int(len(intens[intens==1])/2):],intens[intens==2][int(len(intens[intens==2])/2):]))

    model = Sequential()                                      
    model.add(Embedding(int(t.shape[1]/2), 3, input_length=500))
    model.add(Conv1D(filters=3, kernel_size=3, padding='valid', activation='relu', use_bias = True, kernel_constraint = non_neg(), bias_constraint = min_max_norm()))                                 
    model.add(MaxPooling1D(pool_size=2))                       
    model.add(Flatten())                                     
    model.add(Dense(250, activation='relu', batch_input_shape=(None, 500)))                 
    model.add(Dense(3, activation='sigmoid', batch_input_shape=(None, 500)))            
    model.compile(loss='sparse_categorical_crossentropy', optimizer='adam', metrics=['accuracy'])

    t_sample_weight = compute_sample_weight('balanced', y_train)
    cw = compute_class_weight('balanced', np.unique(y_train), y_train)

    t_val_sample_weights = compute_sample_weight('balanced', y_test)

    t_class_weight = {}
    for i in range(len(cw)):
        t_class_weight[i] = cw[i]


    model.fit(x_train, y_train, validation_data=(x_test, y_test, t_val_sample_weights), epochs=15, batch_size=128, verbose=2, sample_weight = t_sample_weight, class_weight = t_class_weight)                                
    # Final evaluation of the model                                    
    scores = model.evaluate(x_train, y_train, verbose=0)
    print("Train Accuracy: %.2f%%" % (scores[1]*100))

    scores = model.evaluate(x_test, y_test, verbose=0)
    print("Test Accuracy: %.2f%%" % (scores[1]*100))

    sample_weight = compute_sample_weight('balanced', intens)
    cw = compute_class_weight('balanced', np.unique(intens), intens)

    class_weight = {}
    for i in range(len(cw)):
        class_weight[i] = cw[i]

    model.fit(t, intens, epochs=15, batch_size=128, verbose=2, sample_weight = sample_weight, class_weight = class_weight)                                
    # Final evaluation of the model                                    
    scores = model.evaluate(t, intens, verbose=0)
    print("Accuracy: %.2f%%" % (scores[1]*100))

    p = model.predict_classes(t, batch_size = 128, verbose = 1) #utilizando predict solamente, obtenemos el puntaje que obtuvo cada sentimiento

    return p

def entrenar_deep_recomendacion(matriz_tweets, hashtags, label_words):
    """Fase de entrenamiento profunda para el sistema de recomendacion
    -param matriz_tweets: matriz que representa las veces que aparece una palabra en un texto
    -param hashtags: codificacion numerica de los hashtags
    -param label_words: lista de hashtags expresadas como palabras"""
    if matriz_tweets.shape[1] > 500:
        max_words = 500
    else:
        max_words = matriz_tweets.shape[1]

    matriz_tweets = sequence.pad_sequences(matriz_tweets, maxlen=max_words)

    if matriz_tweets.shape[0] > 10:
        folds = 10
    else:
        folds = 2 

    cv = cross_validation.KFold(len(matriz_tweets), n_folds=folds) #realizamos validacion cruzada para ver como esta funcionando nuestro clasificador, es decir, ver si podemos encontrar alguna configuracion de parametros que nos de un resultado mas exacto
    sum_accuracy = 0                                         
    sum_average_precision = 0                                        
    sum_f1 = 0                                                       
    sum_precision = 0         
    sum_recall = 0 
    sum_roc_auc = 0             
    k = 0 
    model = Sequential()                                      
    model.add(Embedding(5000, len(np.unique(hashtags)), input_length=max_words))
    model.add(Conv1D(filters=len(np.unique(hashtags)), kernel_size=3, padding='valid', activation='relu', use_bias = True, kernel_constraint = non_neg(), bias_constraint = min_max_norm()))                                 
    model.add(MaxPooling1D(pool_size=2))                       
    model.add(Flatten())                                     
    model.add(Dense(250, activation='relu', batch_input_shape=(None, max_words)))                 
    model.add(Dense(len(np.unique(hashtags)), activation='sigmoid', batch_input_shape=(None, max_words)))            
    model.compile(loss='sparse_categorical_crossentropy', optimizer='adam', metrics=['accuracy'])
    try:
        for traincv, testcv in cv:
            model.fit(matriz_tweets[traincv[0]:traincv[len(traincv)-1]], hashtags[traincv[0]:traincv[len(traincv)-1]])
            y_true = []
            y_pred = []
            for i in range(len(testcv)):
                y_true.append(label_words[hashtags[testcv[i]]])
                y_pred.append(label_words[model.predict_classes(np.resize(matriz_tweets[testcv[i]], (1, max_words)))])
            acc = metrics.accuracy_score(y_true, y_pred)
            sum_accuracy += acc
            k += 1
            print(str(k) + ')exactitud: ' + str(acc))
            print('Clases utilizadas: ' + str(y_true))
            print('Predicciones: ' + str(y_pred))
            print(' ')
        print ('EXACTITUD: ' + str(sum_accuracy/k))
        model.fit(matriz_tweets, hashtags)
    except Exception:
        pass
    return model

def predict_tags(model, test, targs, hashtags):
    """Prediccion de recomendaciones en el sistema de recomendacion
    -param model: instancia del modelo de recomendacion entrenado
    -param test: matriz de palabras
    -param targs: lista de hashtags expresadas como numeros
    -param hashtags: lista de hashtags expresadas como palabras"""
    if test.shape[1] > 500:
        max_words = 500
    else:
        max_words = test.shape[1]

    test = sequence.pad_sequences(test, maxlen=max_words) 

    y_true = []                                                      
    y_pred = []                                                           
                                                                     
    #calculamos la exactitud total de la clasificacion para saber si la recomendacion es optima   
    for i in range(len(test)):                                       
        y_true.append(hashtags[targs[i]]) #guardamos en la lista los hashtags que utilizamos para clasificar                                 
        y_pred.append(np.array(hashtags)[model.predict_classes(np.array([test[i]]))]) #guardamos los hashtags que se pueden recomendar
    print('Clases utilizadas: ' + str(y_true))                             
    print('Predicciones: ' + str(y_pred))                        
    print('') 

    return model, y_true, y_pred

def recomendar_deep(model, test, targs, hashtags, rows, encoder):                                                       
    """Fase de recomendacion del sistema de recomendacion
    -param model: instancia del modelo de recomendacion entrenado
    -param test: matriz de palabras
    -param targs: lista de hashtags expresadas como numeros
    -param hashtags: lista de hashtags expresadas como palabras
    -param rows: lista de textos con sus hashtags
    -param encoder: instancia de codificacion de hashtags"""
    model, y_true, y_pred = predict_tags(model, test, targs, hashtags)
    hashtags = np.unique(hashtags)
                                                                     
    total_accuracy = metrics.accuracy_score(encoder.transform(y_true), encoder.transform(y_pred)) #calculamos la exactitud total de la prediccion, si es alta (mayor que 0.5) quiere decir que si hay hashtags que se pueden recomendar con exactitud
    print("Exactitud Total: " + str(total_accuracy))                  
    print('')                                                        
    #calculamos la exactitud top3 de nuestros datos                                                                   
    correctly_classified = 0         
    if test.shape[1] > 500:
        max_words = 500
    else:
        max_words = test.shape[1]    
    test = sequence.pad_sequences(test, maxlen=max_words)     
    probas = model.predict_proba(test)                                   
    for i in range(len(test)):                                       
        dist = probas[i]                                                                  
        predicted_probs = []                                         
        for label in range(len(hashtags)-1): 
            predicted_probs.append((hashtags[label], dist[label]))        
        predicted_probs = sorted(predicted_probs, key=itemgetter(1), reverse=True)                                                        
        #si la clase correcta esta entre la exactitud optima, sumamos un punto por clasificacion correcta                                             
        for j in range(3):                                           
            if hashtags[targs[i]] in predicted_probs[j][0]:                  
                correctly_classified += 1                            
                break                                                
    print("Top3 Exactitud Total: " + str(float(correctly_classified)/float(len(test))))                                                    
    print('') 

    #calculamos la precision de clasificacion para cada hashtag
    for hashtag in hashtags:
        print (hashtag)
        hashtag = hashtag.replace('#', '')
        print (hashtag)
        validation_set_filtered = []
        for row in rows:
            if hashtag in row[1] or hashtag.lower() in row[1]:
                validation_set_filtered.append(row)
        y_true_h = []
        y_pred_h = []
        vmt = []                         
        for i in validation_set_filtered:
            vmt.append(i[0])
        vml = []                         
        for i in validation_set_filtered:
            vml.append(i[1])
        print (vml)
        label_index = np.where(np.array(vml[0]) == hashtags)[0][0]                  
        for i in range(len(vml)):
            vml[i] = label_index
        v_mt = count.fit_transform(vmt).toarray()
        for i in range(len(validation_set_filtered)):
            y_true_h.append(vml[i])
            y_pred_h.append(model.predict_classes(np.resize(v_mt[i], (1, test.shape[1]))))
        precision_h = metrics.accuracy_score(y_true_h, y_pred_h)
        print('#' + hashtag + ': precision = ' + str(precision_h) +
              ' (support = ' + str(len(validation_set_filtered)) + ')')
        print('')

    #calculamos la precision Top3 para cada hashtag
    recommend = []
    for hashtag in hashtags:
        hashtag = hashtag.replace('#', '')
        validation_set_filtered = []
        for row in rows:
            if hashtag in row[1] or hashtag.lower() in row[1]:
                validation_set_filtered.append(row)
        vmt = []                         
        for i in validation_set_filtered:
            vmt.append(i[0])
        vml = []                         
        for i in validation_set_filtered:
            vml.append(i[1])
        label_index = np.where(np.array(vml[0]) == hashtags)[0][0]                  
        for i in range(len(vml)):
            vml[i] = label_index
        v_mt = count.fit_transform(vmt).toarray()
        for i in range(len(v_mt)):
            dist = model.predict_proba(np.resize(v_mt[i], (1, test.shape[1])))
            predicted_probs = []
            for label in range(len(hashtags)):
                predicted_probs.append((hashtags[label], dist[0][label]))
            predicted_probs = sorted(predicted_probs, key=itemgetter(1), reverse=True)
            #if the correct label is in the top3 of our classifier, count as correctly classified
            correctly_classified = 0
            for j in range(3):
                if validation_set_filtered[i][1] in predicted_probs[j][0]:
                    correctly_classified += 1
                    break
        try:                                                         
            print('#' + hashtag + ': top3 precision = ' + str(correctly_classified/len(validation_set_filtered)) +                        
              ' (support = ' + str(len(validation_set_filtered)) + ')')               
            print('')
            recommend.append([hashtag, correctly_classified/len(validation_set_filtered), len(validation_set_filtered)]) 
            print('')
        except ZeroDivisionError:
            print ("No hay garantias para recomendar #" + str(hashtag))

    return recommend

def process_all_textos_instagram(textos):
    """Tokenizar todos los textos de las influencias
    -param textos: lista de textos"""
    processed = [[] for i in range(len(textos))]
    Textos = []
    for i in range(len(textos)):
        for j in textos[i]:
            sin_tags = remove_hashtags(j)
            sin_http = remove_hyperlinks(sin_tags)
            sin_usuario = remove_handles(sin_http)
            sin_html = remove_html_entities(sin_usuario)
            alfabeto = remove_punctuation_deep(sin_html)
            tokenizar = process(alfabeto, stopwords = palabras_vacias_es + palabras_vacias_en)
            sin_apostrofe = remove_apostrophes(tokenizar)
            if not is_empty(str(sin_apostrofe)):           
                processed[i].append(sin_apostrofe) 
                Textos.append(textos[i])
    return processed, Textos

Usage = """ python3 deep.py [Opcion recomendacion/sentimiento]                     
        Opciones requeridas:
            -F: recomendar paginas de Facebook         
            -T: recomendar hashtags de Twitter
        Ejemplo:
            python3 deep.py recomendacion -F"""

if __name__ == '__main__':

    if len(sys.argv) < 3:
        print ("\nBad amount of input arguments\n", Usage, "\n")
        sys.exit(1)

    if sys.argv[1] in ('recomendacion') and sys.argv[2] in ('-T'): 
        print(sys.argv)
        if len(sys.argv)  == 3:

            print ("Leyendo archivo de lectura de influencias")                                                                
            from csv_data import *                              
            influencias = csv_read('csv/influencias twitter.csv', tuple)                                                                
            from nlp import *                           
            textos = csv_read('csv/textos_twitter.csv', list)                   
                                
            rows = generar_rasgos_recomendaciones_desde_usuarios(textos)                                                       
            rows = limpiar_test(rows)                                 
            hashtags = []                                             
            for row in rows:                                          
                hashtags.append(row[1])                               
                                                                      
            from collections import Counter                           
            tags = []                                                 
            for i in Counter(hashtags).most_common(30):               
                tags.append(i[0])                                     
                                                                      
            tweets = []                 
            for i in range(len(rows)):                         
                if not rows[i][1] in tags:                      
                    pass                                        
                else:                                           
                    tweets.append(rows[i])                      
                                                                
            hashtags = []                                       
            for row in tweets:                                  
                hashtags.append(row[1])                         
                                                                
            textos = []                                         
            for row in tweets:                                        
                textos.append(row[0])               
                                                                      
            from sklearn.preprocessing import LabelEncoder as le      
            le = le()                                                 
            le.fit(hashtags)                                          
            tags = le.transform(hashtags)                             
            targs = le.classes_                                       
                                                                      
            matriz_tweets = count.fit_transform(textos).toarray()

            model = entrenar_deep_recomendacion(matriz_tweets, tags, hashtags)                                                        
                                                                
            recommend = recomendar_deep(model, matriz_tweets, tags, targs, tweets, le)                                                    
                                                                
            recommend = list(reversed(sorted(recommend, key = lambda x: x[2]))) 

            from csv_data import write_recomendaciones_csv                                                     
                                                                
            write_recomendaciones_csv(recommend, 'csv/recomendaciones_twitter.csv') 

        elif len(sys.argv) > 3:
            print ("Buscando Hashtags")                                                                
            hashtags = sys.argv[3:]
            rows = buscar_hashtags(api, hashtags, 300)                                                              
            from nlp import *                                
            hashtags = []                                             
            for row in rows:                                          
                hashtags.append(row[1])                        
                                                                
            textos = []                                         
            for row in rows:                                        
                textos.append(row[0])                    
                                                                      
            from sklearn.preprocessing import LabelEncoder as le      
            le = le()                                                 
            le.fit(hashtags)                                          
            tags = le.transform(hashtags)                             
            targs = le.classes_                                       
                                                                      
            matriz_tweets = count.fit_transform(textos).toarray()

            model = entrenar_deep_recomendacion(matriz_tweets, tags, hashtags)                                                        
                                                                
            recommend = recomendar_deep(model, matriz_tweets, tags, targs, rows, le)                                                    
                                                                
            recommend = list(reversed(sorted(recommend, key = lambda x: x[2]))) 
                                                      
            from csv_data import write_recomendaciones_csv                                                     
                                                                                                                          
            write_recomendaciones_csv(recommend, 'csv/recomendaciones_twitter.csv') 

    if (sys.argv[1] in 'sentimiento') and (sys.argv[2] in '-T'):
            print ("Leyendo archivo de lectura de influencias")                                                                
            from csv_data import csv_read                          
            influencias = csv_read('csv/influencias twitter.csv', tuple)                                                                
            from nlp import * 
                       
            textos = csv_read('csv/textos_twitter.csv', list)  
            tokens = process_all_textos(textos)                                 
                                                
            mt = gen_lista(textos)

            print ("Generando matriz de palabras. Esto puede tardar")
                      
            matriz_tweets = count.fit_transform(mt).toarray()

            intensidades = intensidad_de_sentimiento(tokens)

            intens = gen_intens(intensidades) 

            if len(matriz_tweets) > len(intens):
                matriz_tweets = matriz_tweets[:len(intens)]
            else:
                intens = intens[:len(matriz_tweets)]
                
            try:
                sentimientos = CNN_sentimiento(matriz_tweets, intens)  
            except Exception as e:
                test = np.load('test1.npy')
                test = test.T[:matriz_tweets.shape[1]].T
                i = [0 for i in range(len(test))]
                matriz_tweets = np.vstack((matriz_tweets,test))
                intens = np.append(intens,i)
                sentimientos = CNN_sentimiento(matriz_tweets, intens)  

                
            from csv_data import write_sentiments_csv   
            write_sentiments_csv(mt, sentimientos, 'csv/sentimientos_twitter.csv')                                            

    if sys.argv[1] in ('recomendacion') and sys.argv[2] in ('-F'):
            print ("Buscando Textos") 
            users = csv_read('csv/egos_facebook.csv', list)   
            users = [str().join(i) for i in users]

            textos = eval(csv_read('csv/textos_facebook.csv', list)[0][0]) 
            tokens = process_vector(textos)                                                               

            from sklearn.preprocessing import LabelEncoder as le                                                           
            from nlp import *                              
                                                                      
            matriz_posts = count_i.fit_transform(tokens).toarray()

            targets = []
            for i in matriz_posts:
                targets.append(random.choice(users))
            rows = []
            for i in range(len(matriz_posts)):
                rows.append([tokens[i],targets[i]])
                
            le = le()
            tags = le.fit_transform(targets)


            model = entrenar_deep_recomendacion(matriz_posts, tags, targets)                                                        
                                                                
            recommend = recomendar_deep(model, matriz_posts, tags, targets, rows, le)                                                    
                                                                
            recommend = list(reversed(sorted(recommend, key = lambda x: x[1]))) 

            print ("La cantidad de candidatos es " + str(len(recommend)))
            print ("Guardando resultados finales")

            recommend = recommend[:5]
                                     
            from csv_data import write_recomendaciones_csv                                                     
                                                                                                                          
            write_recomendaciones_csv(recommend, 'csv/recomendaciones_facebook.csv')  

    if sys.argv[1] in ('sentimiento') and sys.argv[2] in ('-F'):
            print ("Buscando Textos")    
            from csv_data import csv_read
            textos = eval(csv_read('csv/textos_facebook.csv', list)[0][0]) 

            tokens = process_vector(textos)                                 
                                                
            mt = gen_lista(textos)

            print ("Generando matriz de palabras. Esto puede tardar")
                      
            matriz_tweets = count.fit_transform(mt).toarray()

            intensidades = intensidad_de_sentimiento(tokens)

            intens = gen_intens(intensidades) 

            if len(matriz_tweets) > len(intens):
                matriz_tweets = matriz_tweets[:len(intens)]
            else:
                intens = intens[:len(matriz_tweets)]

            from csv_data import write_sentiments_csv   
            write_sentiments_csv(mt, intens, 'csv/sentimientos_facebook.csv')

    if sys.argv[1] in ('sentimiento') and sys.argv[2] in ('-LI'):
            print ("Buscando Textos")    
            from csv_data import csv_read
            textos = csv_read('csv/textos_linkedin.csv', list) 

            tokens = process_all_textos(textos)

            print ("Generando matriz de palabras. Esto puede tardar")
                      
            matriz_tweets = count_i.fit_transform(textos[0]).toarray()
            intensidades = intensidad_de_sentimiento_linkedin(tokens[0])
            from csv_data import write_sentiments_csv   
            write_sentiments_csv(matriz_tweets, intensidades, 'csv/sentimientos_linkedin.csv')

    if sys.argv[1] in ('sentimiento') and sys.argv[2] in ('-I'):
            print ("Buscando Textos")    
            from csv_data import csv_read
            textos = csv_read('csv/textos_instagram.csv', list) 

            tokens, textos = process_all_textos_instagram(textos)
            print ("Generando matriz de palabras. Esto puede tardar")
            matriz_tweets = count_i.fit_transform(textos[0]).toarray()
            intensidades = intensidad_de_sentimiento_linkedin(tokens[0])
            try:
                sentimientos = CNN_sentimiento(matriz_tweets, intens)  
            except Exception as e:
                test = np.load('test1.npy')
                test = test.T[:matriz_tweets.shape[1]].T
                i = [0 for i in range(len(test))]
                matriz_tweets = np.vstack((matriz_tweets,test))
                intensidades = np.append(intensidades,i)
                sentimientos = CNN_sentimiento(matriz_tweets, intensidades)  
            from csv_data import write_sentiments_csv   
            write_sentiments_csv(textos[0][:len(intensidades)], sentimientos, 'csv/sentimientos_instagram.csv')

    if sys.argv[1] in ('recomendacion') and sys.argv[2] in ('-I'):
        print ("Buscando Textos")    
        from csv_data import csv_read
        textos = csv_read('csv/textos_instagram.csv', list) 

        tokens, textos = process_all_textos_instagram(textos)
        print ("Generando matriz de palabras. Esto puede tardar")
        matriz_posts = count_i.fit_transform(textos[0]).toarray() 
        tags = csv_read('csv/palabras_instagram.csv', list) 
        tags = [i[0] for i in tags[1:]]
        from sklearn.preprocessing import LabelEncoder as le                                                           
        from nlp import * 

        targs = [i for i in range(len(tags))]
                                                                  
        model = entrenar_deep_recomendacion(matriz_posts, targs, tags)                                                              

        rows = []
        for i in range(len(targs[:len(textos)])):
            rows.append([textos[i], targs[:len(textos)][i]])
            
        le = le()
        tags = le.fit_transform(targs)

        recommend = recomendar_deep(model, matriz_posts, tags, targs, rows, le)                                                    
                                                                
        recommend = list(reversed(sorted(recommend, key = lambda x: x[1]))) 

        print ("La cantidad de candidatos es " + str(len(recommend)))
        print ("Guardando resultados finales")

        recommend = recommend[:5]

        from csv_data import write_recomendaciones_csv                                                     
                                                                                                                          
        write_recomendaciones_csv(rows, 'csv/recomendaciones_instagram.csv') 

    if sys.argv[1] in ('recomendacion') and sys.argv[2] in ('-LI'):
        print ("Buscando Textos")    
        from csv_data import csv_read
        textos = csv_read('csv/textos_linkedin.csv', list) 

        tokens = process_all_textos(textos)

        print ("Generando matriz de palabras. Esto puede tardar")
                      
        matriz_tweets = count_i.fit_transform(textos[0]).toarray()

        from csv_data import write_sentiments_csv   
        from sklearn.preprocessing import LabelEncoder as le 
        influencias = csv_read('csv/influencias linkedin.csv', tuple)  
        influencias = [i[0] for i in influencias] 
        le = le()
        le.fit(influencias)              
        Tags = le.transform(influencias)
        targs = le.classes_                                       

        rows = []
        tags = []
        for i in range(0,len(textos[0])):
            tag = choice(Tags)
            rows.append((textos[0][i], targs[tag]))
            tags.append(tag)
                                                                  
        model = entrenar_deep_recomendacion(matriz_tweets, tags, targs)                                                              
        recommend = recomendar_deep(model, matriz_tweets, tags, targs, rows, le)                                                    
                                                                
        recommend = list(reversed(sorted(recommend, key = lambda x: x[1]))) #para linkedin recomendamos en base a la precision top en lugar del soporte, dado que contamos con mucha menos data, por ende tenemos que rankear los datos en base a probabilidades

        print ("La cantidad de candidatos es " + str(len(recommend)))

        print ("Guardando resultados finales")

        recommend = recommend[:5]
                                     
        from csv_data import write_recomendaciones_csv                                                     
                                                                                                                          
        write_recomendaciones_csv(recommend, 'csv/recomendaciones_linkedin.csv') 
