#!/usr/bin/python3
# -*- coding: utf-8 -*-

import sys
from login import login
from redes import *
import networkx as nx
from csv_data import *
from operator import itemgetter
import time
import numpy

api = login()

def influencia_total(saliente, entrante):
    """Calcula la influencia total de los usuarios en Twitter. La influencia esta medida en impacto para ser mas precisos con la diferencia entre los usuarios mas influyentes y los menos influyentes
    -param saliente: influencia saliente de los usuarios
    -param entrante: influencia entrante de los usuarios"""
    influencia_impacto = {}
    for i in entrante.keys():
        try:                                               
            influencia_impacto[api.get_user(i).screen_name] = (saliente[i] / entrante[i]) #influencia = saliente/entrante
        except tweepy.error.TweepError:                                                                       
            print ("Se ha superado el limite de busquedas, entrando a tiempo de espera")                                            
            time.sleep(60*20)                                                                       
            continue
    return list(reversed(sorted(influencia_impacto.items(), key = itemgetter(1))))

def influencia_total_facebook(saliente, entrante):
    """Calcula la influencia total de los usuarios en la red. La influencia esta medida en impacto para ser mas precisos con la diferencia entre los usuarios mas influyentes y los menos influyentes
    -param saliente: influencia saliente de los usuarios
    -param entrante: influencia entrante de los usuarios"""
    influencia_impacto = {}
    for i in entrante.keys():                                              
        influencia_impacto[i] = (saliente[i] / entrante[i]) #influencia = saliente/entrante
    return list(reversed(sorted(influencia_impacto.items(), key = itemgetter(1))))

def get_influencias(influencias):
    """Funcion para saber los nombres de las influencias cuando la lista contiene IDs de usuario
    -param influencias: listado de influencias"""
    users = []
    for i in influencias:
        users.append((api.get_user(i[0]).screen_name, i[1]))
    return users

Usage = """python3 influencias.py [-F/-T/-LI]                     
        Opciones requeridas:
            -F: encontrar influencias con archivo de gráfico de red de Facebook         
            -T: encontrar influencias con archivo de gráfico de red de Twitter
            -LI: encontrar influencias con archivo de gráfico de red de LinkedIn
        Ejemplo:
            python3 influencias.py -F"""

if __name__ == '__main__':

    if len(sys.argv) < 2:
        print ("\nBad amount of input arguments\n", Usage, "\n")
        sys.exit(1)

    if sys.argv[1] in ('-T'):
        grafico_egos = nx.read_gexf('redes/egos_twitter.gexf')

    if sys.argv[1] in ('-I'):
        grafico_egos = nx.read_gexf('redes/egos_instagram.gefx')

    if sys.argv[1] in ('-F'):
        grafico_egos = nx.read_gexf('redes/egos_facebook.gefx')

    if sys.argv[1] in ('-LI'):
        grafico_egos = nx.read_gexf('redes/egos_linkedin.gefx')

    lider = list(reversed(sorted(list(grafico_egos.degree().__iter__()), key=lambda x: x[1])))[0][0] 

    influencia_entrante = nx.katz_centrality_numpy(grafico_egos, weight = lider, normalized = True) #forma de medir la centralidad de autovector, cuanta informacion puede recibir un nodo de otro (e_ji)

    influencia_saliente = nx.katz_centrality_numpy(grafico_egos.reverse(), weight = lider, normalized = True) #forma de medir la centralidad de autovector, cuanta informacion puede dar un nodo a otro (e_ij)

    nodo_mas_influyente_salida = sorted(influencia_saliente.items(), key=lambda x: x[1])[-1] #nodo que mas recibe informacion
    nodo_mas_influyente_entrada = sorted(influencia_entrante.items(), key=lambda x: x[1])[-1] #nodo que mas envia informacion

    if type(nodo_mas_influyente_salida[0]) == str:
        print ("El usuario mas influyente en la salida es " + nodo_mas_influyente_salida[0])
    else:
        print ("El usuario mas influyente en la salida es " + str(nodo_mas_influyente_salida[0]))

    if type(nodo_mas_influyente_entrada[0]) == str:
        print ("El usuario mas influyente en la entrada es " + nodo_mas_influyente_entrada[0])
    else:
        print ("El usuario mas influyente en la entrada es " + str(nodo_mas_influyente_entrada[0])) 

    #ordenamos las influencias en orden descendiente
    influencia_entrante_descendiente = list(reversed(sorted(influencia_entrante.items(), key = itemgetter(1))))
    influencia_saliente_descendiente = list(reversed(sorted(influencia_saliente.items(), key = itemgetter(1))))

    if type(influencia_saliente_descendiente[0][0]) == str:
        print(influencia_entrante_descendiente)
        print(influencia_saliente_descendiente)
    else:
        uinfluence_entrante = get_influencias(influencia_entrante_descendiente)
        print (uinfluence_entrante)
        uinfluence_saliente = get_influencias(influencia_saliente_descendiente)
        print (uinfluence_saliente)

    if sys.argv[1] in ('-T'):
        influencias = influencia_total(influencia_saliente, influencia_entrante)
        print("Nodo mas influyente en la red es " + str(influencias[0][0]) + " con " + str(influencias[0][1]) + " de impacto")
        csv_save('csv/influencias twitter.csv', influencias)
    if sys.argv[1] in ('-F'):
        influencias = influencia_total_facebook(influencia_saliente, influencia_entrante)
        print("Nodo mas influyente en la red es " + str(influencias[0][0]) + " con " + str(influencias[0][1]) + " de impacto")
        csv_save('csv/influencias facebook.csv', influencias)
    if sys.argv[1] in ('-LI'):
        influencias = influencia_total_facebook(influencia_saliente, influencia_entrante) #utilizamos la misma funcion que la de facebook ya que contamos con los nombres de los usuarios
        print("Nodo mas influyente en la red es " + str(influencias[0][0]) + " con " + str(influencias[0][1]) + " de impacto")
        csv_save('csv/influencias linkedin.csv', influencias)
    if sys.argv[1] in ('-I'):
        influencias = influencia_total_facebook(influencia_saliente, influencia_entrante)
        print("Nodo mas influyente en la red es " + str(influencias[0][0]) + " con " + str(influencias[0][1]) + " de impacto")
        csv_save('csv/influencias instagram.csv', influencias)

