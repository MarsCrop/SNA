from selenium import webdriver
from selenium.webdriver.chrome.options import Options
import time
from bs4 import BeautifulSoup

class Linkedin():
    def scrape_post_profile(self, peoples):
        chrome_options = Options()
        chrome_options.add_argument("--headless")
        chrome_options.add_argument('--no-sandbox')
        driver = webdriver.Chrome(options=chrome_options)
        driver.get('https://www.linkedin.com/login')
        driver.find_element_by_id('username').send_keys('marscrophimself@protonmail.com')
        driver.find_element_by_id('password').send_keys('36727268ThePiper36727268ThePiper')
        driver.find_element_by_xpath("//*[@type='submit']").click()
        textos = []
        for people in peoples:
                    
            profile_url = "https://www.linkedin.com/in/" + str(people[0])
            driver.get(profile_url)

            # #********** Profile Details **************#
            page = BeautifulSoup(driver.page_source,'lxml')
            count = 0        
            for p in page.findAll('p'):
                if count >= len(page.findAll('p'))-4:
                    break                            
                textos.append(p.text)                
                count += 1  
                
        driver.quit()
        return textos
    def getData(self, search_key):
        chrome_options = Options()
        chrome_options.add_argument("--headless")
        chrome_options.add_argument('--no-sandbox')
        driver = webdriver.Chrome(options=chrome_options)
        driver.get('https://www.linkedin.com/login')
        driver.find_element_by_id('username').send_keys('marscrophimself@protonmail.com')
        driver.find_element_by_id('password').send_keys('36727268ThePiper36727268ThePiper')
        driver.find_element_by_xpath("//*[@type='submit']").click()
            
        global data
        data = []

        for no in range(1,6):
            start = "&page={}".format(no) 
            search_url = "https://www.linkedin.com/search/results/people/?keywords={}&origin=SUGGESTION{}".format(search_key,start)
            driver.get(search_url)
            driver.maximize_window()
            for scroll in range(2):
                driver.execute_script("window.scrollTo(0, document.body.scrollHeight);")
                time.sleep(2)
            search = BeautifulSoup(driver.page_source,'lxml')
            peoples = search.findAll('a', attrs = {'data-control-name':'search_srp_result'})
            count = 0
            print("Going to scrape Page {} data".format(no))
            
            for people in peoples:
                count+=1

                if count%2==0:
                    
                    
                    
                    profile_url = "https://www.linkedin.com" + str(people['href'])
                    driver.get(profile_url)

                    # #********** Profile Details **************#
                    page = BeautifulSoup(driver.page_source,'lxml')
                    try:
                        cover = page.find('img', attrs = {'class':'profile-background-image__image relative full-width full-height'})['src']
                    except:
                        cover = 'None'

                    try:
                        profile = page.find("img", attrs = {
                            'class':'lazy-image pv-top-card-section__photo presence-entity__image EntityPhoto-circle-9 loaded'})['src']
                        
                    except:
                        profile = "None"

                    try:
                        title = str(page.find("li", attrs = {'class':'inline t-24 t-black t-normal break-words'}).text).strip()
                    except:
                        title = 'None'
                    try:
                        heading = str(page.find('h2', attrs = {'class':'mt1 t-18 t-black t-normal'}).text).strip()
                    except:
                        heading = 'None'
                    try:
                        loc = str(page.find('li', attrs = {'class':'t-16 t-black t-normal inline-block'}).text).strip()
                    except:
                        heading = 'None'


                    #*******  Contact Information **********#
                    time.sleep(2)
                    driver.get(profile_url + 'detail/contact-info/')

                    info = BeautifulSoup(driver.page_source, 'lxml')
                    details = info.findAll('section',attrs = {'class':'pv-contact-info__contact-type'})
                    try:
                        websites = details[1].findAll('a')
                        for website in websites:
                            website = website['href']
                            
                    except:
                        website = 'None'
                    try:
                        phone = details[2].find('span').text
                    except:
                        phone = 'None'
                    try:
                        email = str(details[3].find('a').text).strip()
                    except:
                        email = 'None'
                    try:
                        connected = str(details[-1].find('span').text).strip()
                    except:
                        connected = 'None'

                    
                    data.append({'profile_url':profile_url,'cover':cover,'profile':profile,'title':title,'heading':heading,'loc':loc,'website':website,'phone':phone,'email':email,'connected':connected,})
            print("!!!!!! Data scrapped !!!!!!")
                
        driver.quit()
    def writeData(self):
        nodes = []
        for i in range(1,len(data)):
           
            nodes.append(data[i]['profile_url'].split('/')[-2])
        return nodes

    def start(self,search_key):
        self.getData(search_key)
        nodes = self.writeData()
        return nodes