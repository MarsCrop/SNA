import requests
from collections import defaultdict
from bs4 import BeautifulSoup
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
import urllib3
import argparse
import sys
import facebook  
from csv_data import *
browsed = False
from linkedinparser import *                                    
from lxml import html                   
import time                             
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities
from selenium.webdriver.firefox.firefox_binary import FirefoxBinary
from selenium.webdriver.firefox.options import Options
import re
def login_facebook(mail, pwd):
    session = requests.Session()
    r = session.get('https://www.facebook.com/', allow_redirects=False)
    soup = BeautifulSoup(r.text)
    action_url = soup.find('form', id='login_form')['action']
    inputs = soup.find('form', id='login_form').findAll('input', {'type': ['hidden', 'submit']})
    post_data = {input.get('name'): input.get('value')  for input in inputs}
    post_data['email'] = mail
    post_data['pass'] = pwd.upper()
    scripts = soup.findAll('script')
    scripts_string = '/n/'.join([script.text for script in scripts])
    datr_search = re.search('\["_js_datr","([^"]*)"', scripts_string, re.DOTALL)
    if datr_search:
        datr = datr_search.group(1)
        cookies = {'_js_datr' : datr}
    else:
        return False
    resp = session.post(action_url, data=post_data, cookies=cookies, allow_redirects=False)
    if resp.status_code == 200:
        return session
    else:
        return False

def instagram_login():
    """Inicio de sesion en LinkedIn. Se debe introducir el nombre de usuario y la password en los campos correspondientes"""      
    client = requests.Session()
                                         
    HOMEPAGE_URL = 'https://www.instagram.com/'          
    LOGIN_URL =  HOMEPAGE_URL + 'accounts/login/ajax/'
    USERNAME = 'mdevlopmentaccount.yahoo.com'
    PASSWD = '36727268'
    USER_AGENT = 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko)\
 Chrome/59.0.3071.115 Safari/537.36'
                                       
    client.headers = {'user-agent': USER_AGENT}
    client.headers.update({'Referer': LOGIN_URL})
    try:
        req = client.get(LOGIN_URL)
        client.headers.update({'X-CSRFToken': req.cookies['csrftoken']})                                                          
        login_data = {'username': USERNAME, 'password': PASSWD}
        login = client.post(LOGIN_URL, data=login_data, allow_redirects=True)                                                                 
        client.headers.update({'X-CSRFToken': login.cookies['csrftoken']})                        
        cookies = login.cookies                 
    except requests.exceptions.ConnectionError:
        print("Connection refused")

    
    return client 


def scrape_post_profile(username,session):
    """Buscar posts de Facebook"""
    source = session.get('https://www.facebook.com/pg/'+username+'/posts/').text
    soup = BeautifulSoup(source)

    soup.find_all('div', {'data-testid': 'post_message'})       
    posts = []                                                                                              
    for post in soup.find_all('div', {'data-testid': 'post_message'}):
        posts.append(post.text)    
    return posts

def browse_friends(username, session):
    """Funcion que permite realizar la busqueda de amigos de mas de un usuario de Facebook
    -param username: nombre del usuario
    -param session: sesion de Facebook"""                                       
    source = session.get('https://www.facebook.com/pg/'+username+'/posts/').text
    soup = BeautifulSoup(source)

    soup.find_all('div', {'data-testid': 'post_message'})  
    node = []
    for i in soup.findAll('span'):
        if 'ajaxify' in str(i):
            node.append(i)
    nodes = []
    for Node  in set(node):
        nodes.append([username,str(Node).split('/')[1]]) 
                          
    return nodes
    
import tweepy

def login():
    """Inicio de sesion de Twitter. Se deben introducir las credenciales de autorizacion para poder iniciar la sesion"""   
    consumer_key =  'sf3YmYMIuJD6jN0SbCVHvRd9B' #clave              
    consumer_secret = 'Me02xxOB8yDgccvUz8vZU2YiwLxzDYhjgPIhCKW6s9JDi4zLJQ' #secreto                                                      
    access_token = '1179486115-1VXU1U01jxTbrogNiPoah9pKxyboPBNuzOFLhK6' #token de acceso                                  
    access_token_secret = 'KGMEe0BOMgFYfBHOcTnSm9ZBm1u3aA7LTM2dwJouRGPar' #secreto de acceso  

    #accedemos normalmente
    auth = tweepy.OAuthHandler(consumer_key, consumer_secret)
    auth.set_access_token(access_token, access_token_secret)

    api = tweepy.API(auth)
    return api
