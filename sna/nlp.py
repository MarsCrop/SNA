# -*- coding: utf-8 -*-

import sys
import tweepy
from login import *
import numpy as np
from nltk import FreqDist
from nltk.tokenize import TweetTokenizer
from nltk.tokenize.casual import remove_handles
from nltk.corpus import stopwords
from string import punctuation
from nltk import AffixTagger, UnigramTagger, BigramTagger
from nltk.collocations import BigramAssocMeasures as bigram 
from nltk.collocations import BigramCollocationFinder as bigram_finder
from nltk.collocations import TrigramAssocMeasures as trigram 
from nltk.collocations import TrigramCollocationFinder as trigram_finder
import string                                      
from nltk import word_tokenize                                   
from nltk.corpus import stopwords, treebank                     
from nltk import BigramTagger, UnigramTagger, AffixTagger
from sklearn.metrics.pairwise import cosine_similarity
from sklearn.feature_extraction.text import CountVectorizer
import matplotlib.pyplot as plt
import re
import time
from csv_data import *
import os
from redes import *

os.environ['KERAS_BACKEND'] = 'theano'

api = login()

def instagram_posts(profiles):                                                                  
    for prof in profiles:
        url = 'https://www.instagram.com/'+prof[0]+'/?hl=en'
        USER_AGENTS = ['Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36']
        instagram = InstagramScraper(url,USER_AGENTS)
        post_metrics = instagram.post_metrics()
        posts = []            
        for m in post_metrics: 
            i_id = 'https://www.instagram.com/p/'+str(m['shortcode'])                              
            i_comments = int(m['edge_media_to_comment']['count'])
            if i_comments > 1:                        
                posts.append(i_id)             
    t = []
    for post in posts:
        response = requests.get(post,headers={'User-Agent': USER_AGENTS[0]})               
        response.raise_for_status() 
        soup = BeautifulSoup(response.text, 'html.parser')
        body = soup.find('body')
        script_tag = body.find('script')
        raw_string = script_tag.text.strip().replace('window._sharedData =', '').replace(';', '')
        metrics = json.loads(raw_string)
        metrics = metrics['entry_data']['PostPage'][0]['graphql']['shortcode_media']['edge_media_preview_comment']['edges']

        for metric in metrics:
            comment = metric['node']['text']
            t.append(comment) 
    return t

def browse_post(username, n, session): 
    """Funcion que permite obtener n cantidad de posts de un usuario de Facebook
    -param username: nombre del usuario
    -param n: cantidad de posts por obtener"""
    session.get('https://m.facebook.com/' + username)                                    
    page_resp = session.page_source      
                                                                      
    soup = BeautifulSoup(page_resp) 

    timeline = None
    for links in soup.findAll('a', href = True):
        if "?v=timeline&lst" in links['href']:
            timeline = (links['href'])
        if timeline == None:
            if "?sectionLoadingID=m_timeline_loading_div_" in links['href']:
                timeline = links['href']

    try:
        session.get('https://m.facebook.com/' + timeline)    
    except TypeError: 
        return []

    page_resp = session.page_source  
    soup = BeautifulSoup(page_resp)

    try:
        stories = []
        count = 0
        while count <= n:
            for links in soup.findAll('a', href = True):
                if 'story.php' in (links['href']):
                    stories.append(links['href'])
            if stories == [] or (len(stories) == 1 and 1 < count):
                break
            for links in soup.findAll('a', href = True):
                if username+'?sectionLoadingID' in (links['href']):
                    browse_more = links['href']
            page = 'http://m.facebook.com/' + browse_more
            session.get(page)
            page_resp = session.page_source
            soup = BeautifulSoup(page_resp)
            count += 1

        text = []
        for i in range(len(stories)):
            page = 'http://m.facebook.com/' + stories[i]
            session.get(page)
            page_resp = session.page_source
            soup = BeautifulSoup(page_resp)
            post = []
            for i in soup.findAll('p'):
                post.insert(0, i.text)
            text.append(str().join(post))
    except Exception:   
        print(username + " no permite recolección de su timeline")
        return []
    return list(set(text))

#recolectamos los últimos tweets de los usuarios más influyentes
def recolectar_tweets(influencias):
    """Funcion que permite recolectar hasta 100 tweets de cada influencia
    -param influencias: listado de influencias"""
    tweets = []                   
    for i in range(len(influencias)):
        try:                                                           
            tweets.append(api.search(q='to: '+influencias[i][0],screen_name = influencias[i][0],count = 800, include_rts = True))
        except Exception as e:                                      
            print (e)
            continue #si el usuario no permite recolectar sus tweets pasamos al siguiente usuario
    return tweets

def text_from_tweets(tweets):  
    """Los tweets son obtenidos como objetos de tweets. Nosotros debemos convertir los objetos a textos analizables
    -param tweets: tweets obtenidos"""           
    textos = [[] for i in range(len(tweets))]
    for i in range(len(tweets)):
        for j in tweets[i]:     
            textos[i].append(j.text) #de la recoleccion de tweets solamente precisamos el texto
    return textos 

def process(text, stopwords):
    """Tokenizados basico de un tweet
    -param text: textos obtenidos
    -param stopwords: lista de palabras vacias""" 
    text = text.lower()
    tokens = TweetTokenizer(strip_handles = True, reduce_len = True).tokenize(text)
    tokens = normalize(tokens)
    return [tok for tok in tokens if tok not in stopwords and not tok.isdigit()]

def normalize(tokens):
    """Normalizamos contracciones y palabras mal escritas o que se puedan procesar mal. El token map debe de actualizarse por cada palabra nueva sin normalizar aparezca
    -param tokens: tokens""" 
    token_map = {
        "i'm": "i am",
        "you're": "you are",
        "it's": "it is",
        "we're": "we are",
        "we'll": "we will",
        "ud": "usted",
        "uds": "ustedes",
        "hs": "horas",
        "palante": "para adelante",
        'See more':'',
        'see more':'',
        'name':'',
        'company':'',
        "patras": "para atras",
        "tambin": "tambien",
        "qu": "que",
        "maana": "mañana",
        "adems": "además",
        "contina": "continúa",
        "aos": "años",
        "despaciito": "despacito",
        "despacitoo": "despacito",
        "vofi": "vos fijate",
        "x": "por",
        "ke": "que",
    }
    for tok in tokens:
        if tok in token_map.keys():
            for item in token_map[tok].split():
                yield item
        else:
            yield tok

def is_empty(tweet):
    """Condicion: un tweet no puede contener informacion poco importante
    -param tweet: tweet""" 
    return tweet in ''

def frecuencias_terminos(tokens, filename):
    """Funcion para obtener la cantidad de repeticiones de una palabra en todos los textos
    -param tokens: lista de tokens
    -param filename: nombre de archivo para guardar el grafico de barras"""                        
    term_freq = FreqDist()
    for i in tokens:
        for j in i:
            term_freq.update(FreqDist(j.split())) 
    y = [count for tag, count in term_freq.most_common(30)]
    x = range(1, len(y)+1)
    palabras = term_freq.most_common(30)
    plt.bar(x, y)
    for i in range(len(palabras)):
        if i == 0:             
            plt.text(x[i], y[i], palabras[i][0], color='red')
        else:
            plt.text(x[i], y[i], palabras[i][0], color = 'red', rotation = 'vertical', verticalalignment = 'bottom')
    plt.title("Frecuencias de los terminos")
    plt.ylabel("Frecuencia")
    plt.savefig(filename)
    return palabras

def frecuencias_terminos_instagram(tokens,fname): 
    """Funcion para obtener la cantidad de repeticiones de una palabra en todos los textos de LinkedIn
    -param tokens: lista de tokens"""                       
    term_freq = FreqDist()
    for i in tokens:
        term_freq.update(FreqDist(i.split())) 
    y = [count for tag, count in term_freq.most_common(30)]
    x = range(1, len(y)+1)
    palabras = term_freq.most_common(30)
    plt.bar(x, y)
    for i in range(len(palabras)):
        if i == 0:             
            plt.text(x[i], y[i], palabras[i][0], color='red')
        else:
            plt.text(x[i], y[i], palabras[i][0], color = 'red', rotation = 'vertical', verticalalignment = 'bottom')
    plt.title("Frecuencias de los terminos")
    plt.ylabel("Frecuencia")
    plt.savefig(fname)
    return palabras

def frecuencias_terminos_linkedin(tokens): 
    """Funcion para obtener la cantidad de repeticiones de una palabra en todos los textos de LinkedIn
    -param tokens: lista de tokens"""                       
    term_freq = FreqDist()
    for i in tokens:
        term_freq.update(FreqDist(i.split())) 
    y = [count for tag, count in term_freq.most_common(30)]
    x = range(1, len(y)+1)
    palabras = term_freq.most_common(30)
    plt.bar(x, y)
    for i in range(len(palabras)):
        if i == 0:             
            plt.text(x[i], y[i], palabras[i][0], color='red')
        else:
            plt.text(x[i], y[i], palabras[i][0], color = 'red', rotation = 'vertical', verticalalignment = 'bottom')
    plt.title("Frecuencias de los terminos")
    plt.ylabel("Frecuencia")
    plt.savefig('palabras/terminos_linkedin.png')
    return palabras

def tag_to_keep(tag):
    """Funcion que filtra palabras de acuerdo al tipo de palabra. Se utiliza el parámetro TAGS_TO_KEEP que contiene los tags asociados a las palabras que hay que almacenar (por defecto se utilizan solamente los adjetivos)
    -param tag: tag de una palabra""" 
    for t in TAGS_TO_KEEP:
            if t in tag:
                return True
    return False

def remove_multiple_spaces(tweet):
    """Remover espacios en un texto o tweet para que el espaciado no supere el margen establecido por un backspacer
    -param tweet: tweet""" 
    return ' '.join(tweet.split())

non_symbols = ['á', 'é', 'í', 'ó', 'ú', 'ñ'] #prevenir el filtrado de símbolos tomando los caracteres en español

def filter_symbols(char):
    """Filtra simbolos que aparezcan en un texto o palabra
    -param char: caracter""" 
    if ord(char) in range(65, 91) or ord(char) in range(97, 123) or char == ' ' or (char in non_symbols):
        return char
    else:
        return ''

def apostrophe_filter(char):
    """Filtro de apostrofes
    -param char: caracter""" 
    if not char == '\'':
        return char
    else:
        return ''

def remove_apostrophes(tweet):
    """Remover apostrofes de un texto o tweet
    -param tweet: tweet o texto""" 
    try:                                                               
        return ' '.join(list(filter(apostrophe_filter, tweet)))    
    except UnicodeDecodeError:                                         
        return ' ' 

def remove_punctuation_deep(tweet):
    """Remover todos los signos de puntuacion en un tweet o texto
    -param tweet: tweet o texto""" 
    return ''.join(list(filter(filter_symbols, tweet)))

def remove_html_entities(tweet):
    """Remover posibles entidades en html de un tweet ot texto
    -param tweet: tweet o texto""" 
    word_list = tweet.split()
    words_to_keep = []
    for word in word_list:
        if not word.startswith('&'):
            words_to_keep.append(word)
    return ' '.join(words_to_keep)

def remove_hashtags(tweet):
    """Remover hashtags en un tweet ot texto
    -param tweet: tweet o texto""" 
    word_list = tweet.split()  
    words_to_keep = []
    for word in word_list:
        if not word.startswith('#'):
            words_to_keep.append(word)
    return ' '.join(words_to_keep)

def remove_hyperlinks(tweet):
    """Remover hipervinculos de un texto
    -param tweet: tweet o texto""" 
    word_list = tweet.split()
    words_to_keep = []
    for word in word_list:
        if not 'http' in word:
            words_to_keep.append(word)
    return ' '.join(words_to_keep)

def process_vector(textos):
    """Tokenizar una lista de textos
    -param textos: lista de textos"""
    processed = []
    for j in textos:
        try:
            sin_tags = remove_hashtags(j)
            sin_http = remove_hyperlinks(sin_tags)
            sin_usuario = remove_handles(sin_http)
            sin_html = remove_html_entities(sin_usuario)
            alfabeto = remove_punctuation_deep(sin_html)
            tokenizar = process(alfabeto, stopwords = palabras_vacias_es + palabras_vacias_en)
            sin_apostrofe = remove_apostrophes(tokenizar)
            if not is_empty(str(sin_apostrofe)):                
                processed.append(sin_apostrofe) 
        except Exception as e:
            return 'silence'
    return processed

def process_all_textos(textos):
    """Tokenizar todos los textos de las influencias
    -param textos: lista de textos"""
    processed = [[] for i in range(len(textos))]
    for i in range(len(textos)):
        for j in textos[i]:
            sin_tags = remove_hashtags(j)
            sin_http = remove_hyperlinks(sin_tags)
            sin_usuario = remove_handles(sin_http)
            sin_html = remove_html_entities(sin_usuario)
            alfabeto = remove_punctuation_deep(sin_html)
            tokenizar = process(alfabeto, stopwords = palabras_vacias_es + palabras_vacias_en)
            sin_apostrofe = remove_apostrophes(tokenizar)
            if not is_empty(str(sin_apostrofe)):           
                processed[i].append(sin_apostrofe) 
    return processed

def bigrama(tokens):
    """Obtener bigramas mas comunes en una lista de tokens
    -param tokens: lista de tokens"""     
    bigram_medidas = bigram()
    found = []
    for i in range(len(tokens)):
        try:                                
            for j in tokens[i]:                                         
                finder = bigram_finder.from_words(j.split())          
                finder.apply_freq_filter(1) #filtramos los bigramas que hayan aparecido una vez                                                 
            bigrams = list(finder.ngram_fd.keys())     
            if bigrams != []:                                         
                found.append(bigrams)                                 
        except Exception as e:                                        
            continue 
    return found

def trigrama(tokens): 
    """Obtener trigramas mas comunes en una lista de tokens
    -param tokens: lista de tokens"""    
    trigram_medidas = trigram()
    found = []
    for i in range(len(tokens)):
        try:                                
            for j in tokens[i]:                                         
                finder = trigram_finder.from_words(j.split())          
                finder.apply_freq_filter(1) #filtramos los bigramas que hayan aparecido una vez                                                 
            trigrams = list(finder.ngram_fd.keys())                     
            if trigrams != []:                            
                found.append(trigrams)                                
        except Exception as e:                                        
            continue 
    return found

def n_grama(tokens, n):
    """Obtener n-gramas mas comunes en una lista de tokens
    -param tokens: lista de tokens
    -param n: cantidad n de n-grama"""
    vect = CountVectorizer(ngram_range=(n-1,n))
    analyzer = vect.build_analyzer()
    found = []
    for i in range(len(tokens)):
        try:                 
            for j in tokens[i]:                                        
                analyzer(j)        
                listNgramQuery = analyzer(j)
                listNgramQuery.reverse()
            Ngrams = []
            for m in listNgramQuery:
                if len(m.split()) == n:
                    Ngrams.append(m)
            ngrams = FreqDist(Ngrams)
            if list(ngrams.keys()) != []:
                found.append(list(ngrams.keys()))
        except Exception as e:
            continue

    return found

def bigrama_linkedin(tokens):
    """Obtener bigramas mas comunes en una lista de tokens obtenidos de LinkedIn
    -param tokens: lista de tokens"""     
    bigram_medidas = bigram()
    found = []
    for i in tokens:                
        try:                                        
            finder = bigram_finder.from_words(i.split())          
            finder.apply_freq_filter(1) #filtramos los bigramas que hayan aparecido una vez
        except ZeroDivisionError:
            pass
    bigrams = list(finder.ngram_fd.keys())
    if bigrams != []:
        found.append(bigrams)
    return found

def trigrama_linkedin(tokens): 
    """Obtener trigramas mas comunes en una lista de tokens obtenidos de LinkedIn
    -param tokens: lista de tokens"""    
    trigram_medidas = bigram()
    found = []
    for i in tokens:                
        try:                                        
            finder = trigram_finder.from_words(i.split())          
            finder.apply_freq_filter(1) #filtramos los bigramas que hayan aparecido una vez
        except ZeroDivisionError:
            pass
    trigrams = list(finder.ngram_fd.keys())
    if trigrams != []:
        found.append(trigrams)
    return found

def n_grama_linkedin(tokens, n):
    """Obtener n-gramas mas comunes en una lista de tokens obtenidos de LinkedIn
    -param tokens: lista de tokens
    -param n: cantidad n de n-grama"""
    vect = CountVectorizer(ngram_range=(n-1,n))
    analyzer = vect.build_analyzer()
    found = []
    for i in tokens:                
        try:                                        
            analyzer(i)        
            listNgramQuery = analyzer(i)
            listNgramQuery.reverse()
        except ZeroDivisionError:
            pass
    Ngrams = []
    for m in listNgramQuery:
        if len(m.split()) == n:
            Ngrams.append(m)
    ngrams = FreqDist(Ngrams)
    if list(ngrams.keys()) != []:
        found.append(list(ngrams.keys()))
    return list(np.unique(found))

puntuacion = list(punctuation) #necesitamos una lista de los signos de puntuacion para tokenizar y limpiar el texto
palabras_vacias_es = stopwords.words('spanish') + puntuacion + ['RT', 'MT', 'cc', 'via', 'rt', 'https', '...', 'http', '\xe2\x80\xa6', '\xc2\xa1', '\xe2\x80\x9c', 'jajaja', '*emoji', 'Jajaja', 'vía', 'httpst', 'pm', 'ms', 'ac', 'httpstco', 'est', 'dec', 'ht', 'ao', 'tomtrsound', 'mmm', 'si', 'c', 'q', ':)', '<3', '#', 'aniita', 'carolina', 'herman', 'sofia', 'valencio', 'n','campos', 'luciana','verónica', 'soria'] #eliminamos las palabras vacias, aquellas que no tienen significado
palabras_vacias_en = stopwords.words('english') + puntuacion + ['RT', 'MT', 'cc', 'via', 'rt', 'https', '...', 'http', '\xe2\x80\xa6', '\xe2\x80\x9c', '\xc2\xa1', 'jajaja', '*emoji', 'Jajaja', 'vía',' httpst', 'pm', 'ms', 'ac', 'httpstco', 'est', 'dec', 'ht', 'ao', 'tomtrsound', 'mmm', 'si', 'c', 'q', ':)', '<3', '#'] #eliminamos las palabras vacias, aquellas que no tienen significado

Usage = """python3 nlp.py [-F/-T/-LI]                     
        Opciones requeridas:
            -F: obtener palabras más utilizadas por influencias en red de Facebook         
            -T: obtener palabras más utilizadas por influencias en red de Twitter
            -LI: obtener palabras más utilizadas por influencias en red de LinkedIn
        Ejemplo:
            python3 nlp.py -F"""

if __name__ == '__main__':

    if len(sys.argv) < 2:
        print ("\nBad amount of input arguments\n", Usage, "\n")
        sys.exit(1)

    if sys.argv[1] in ('-T'):
        influencias = csv_read('csv/influencias twitter.csv', list)    
        tweets = recolectar_tweets(influencias)
        textos = text_from_tweets(tweets)

    if sys.argv[1] in ('-I'):
        influencias = csv_read('csv/influencias instagram.csv', list)    
        posts = instagram_posts(influencias)
        textos_save('csv/textos_instagram.csv', posts)
        tokens = process_vector(posts)
        bigramas = bigrama_linkedin(tokens)
        trigramas = trigrama_linkedin(tokens)                
        pentagramas = n_grama_linkedin(tokens, 5)
        SaveNGrams('csv/bigrama_instagram.csv', bigramas)                
        SaveNGrams('csv/trigrama_instagram.csv', trigramas)  
        SaveNGrams('csv/n_grama_instagram.csv', [pentagramas])    
        palabras = frecuencias_terminos_instagram(tokens, 'palabras/terminos_instagram.png') 
        write_palabras_csv(palabras, 'csv/palabras_instagram.csv')        
        sys.exit(1)   
    if sys.argv[1] in ('-F'):
        influencias = csv_read('csv/influencias facebook.csv', list)
        textos = []
        session = login_facebook('mdevlopmentaccount@yahoo.com', '36727268ThePiper') 
        for i in np.array(influencias)[:,0]:
            print("Obteniendo posts de usuario: " + i)
            textos.append(scrape_post_profile(i,session))

    if sys.argv[1] in ('-LI'):
        influencias = csv_read('csv/influencias linkedin.csv', list)
        obJH = Linkedin()        
        textos = obJH.scrape_post_profile(influencias)
        tokens = process_vector(textos)
        palabras = frecuencias_terminos_linkedin(tokens)
    elif not sys.argv[1] in ('-I'):
        tokens = process_all_textos(textos)
    if sys.argv[1] in ('-F'):
        palabras = frecuencias_terminos(tokens, 'palabras/terminos_facebook.png')
    if sys.argv[1] in ('-T'):
        palabras = frecuencias_terminos(tokens, 'palabras/terminos_twitter.png')
    if sys.argv[1] in ('-T'):
        write_palabras_csv(palabras, 'csv/palabras_twitter.csv')
        textos_twitter_save('csv/textos_twitter.csv', textos)

    if sys.argv[1] in ('-F'):
        write_palabras_csv(palabras, 'csv/palabras_facebook.csv')
        textos_save('csv/textos_facebook.csv', textos)

    if sys.argv[1] in ('-LI'):
        write_palabras_csv(palabras, 'csv/palabras_linkedin.csv')
        textos_save('csv/textos_linkedin.csv', tokens)
    if not sys.argv[1] in ('-LI'):                
        bigramas = bigrama(tokens)
        trigramas = trigrama(tokens)                
        pentagramas = n_grama(tokens, 5)
        if sys.argv[1] in ('-T'): 
            SaveNGrams('csv/bigrama_twitter.csv', bigramas)                
            SaveNGrams('csv/trigrama_twitter.csv', trigramas)  
            SaveNGrams('csv/n_grama_twitter.csv', [pentagramas])
        if sys.argv[1] in ('-F'): 
            SaveNGrams('csv/bigrama_facebook.csv', bigramas)                
            SaveNGrams('csv/trigrama_facebook.csv', trigramas)  
            SaveNGrams('csv/n_grama_facebook.csv', [pentagramas])
            sys.exit(1)

    elif not sys.argv[1] in ('-I'):                
        bigramas = bigrama_linkedin(tokens)
        trigramas = trigrama_linkedin(tokens)                
        pentagramas = n_grama_linkedin(tokens, 5)
        SaveNGrams('csv/bigrama_linkedin.csv', bigramas)                
        SaveNGrams('csv/trigrama_linkedin.csv', trigramas)  
        SaveNGrams('csv/n_grama_linkedin.csv', [pentagramas])