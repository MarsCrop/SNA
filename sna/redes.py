import sys
import tweepy
import numpy as np
from login import *
from collections import defaultdict, Counter
import networkx as nx
import random
import matplotlib.pyplot as plt
from itertools import permutations
from csv_data import *
import numpy
from random import choice
from datetime import datetime
import json
import requests
from bs4 import BeautifulSoup
from networkx.readwrite.gexf import write_gexf
from linkedinparser import *

def save_graph(g,fname):
    pos = nx.spring_layout(g)
    nx.draw(g, pos, node_size=1500, node_color='yellow', font_size=8, font_weight='bold')
    nx.draw_networkx_labels(g, pos, node_size=1500, node_color='yellow', font_size=8, font_weight='bold')
    plt.tight_layout()
    plt.savefig(fname, format="PNG")


class InstagramScraper:
    def __init__(self, url, user_agents=None):
        self.url = url
        self.user_agents = user_agents

    def __random_agent(self):
        if self.user_agents and isinstance(self.user_agents, list):
            return choice(self.user_agents)
        return choice(self.user_agents)

    def __request_url(self):
        try:
            response = requests.get(
                        self.url,
                        headers={'User-Agent': self.__random_agent()})
            response.raise_for_status()
        except requests.HTTPError:
            raise requests.HTTPError('Received non-200 status code.')
        except requests.RequestException:
            raise requests.RequestException
        else:
            return response.text
    @staticmethod
    def extract_json(html):
        soup = BeautifulSoup(html, 'html.parser')
        body = soup.find('body')
        script_tag = body.find('script')
        raw_string = script_tag.text.strip().replace('window._sharedData =', '').replace(';', '')
        return json.loads(raw_string)
    
    def page_metrics(self):
        results = {}
        try:
            response = self.__request_url()
            json_data = self.extract_json(response)
            metrics = json_data['entry_data']['ProfilePage'][0]['graphql']['user']
        except Exception as e:
            raise e
        else:
            for key, value in metrics.items():
                if key != 'edge_owner_to_timeline_media':
                    if value and isinstance(value, dict):
                        value = value['count']
                        results[key] = value
        return results
    def post_metrics(self):
        results = []
        try:
            response = self.__request_url()
            json_data = self.extract_json(response)
            metrics = json_data['entry_data']['ProfilePage'][0]['graphql']['user']['edge_owner_to_timeline_media']['edges']
        except Exception as e:
            raise e
        else:
            for node in metrics:
                node = node.get('node')
                if node and isinstance(node,dict):
                    results.append(node)
        return results

def instagram_login(profile):                                                                  
    url = 'https://www.instagram.com/'+profile+'/?hl=en'
    USER_AGENTS = ['Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36']
    instagram = InstagramScraper(url,USER_AGENTS)    
    post_metrics = instagram.post_metrics()
    posts = []            
    for m in post_metrics: 
        i_id = 'https://www.instagram.com/p/'+str(m['shortcode'])                              
        i_comments = int(m['edge_media_to_comment']['count'])
        if i_comments > 1:                        
            posts.append(i_id)                               
    return posts 

def scrape_users(posts,prof):
    t = []
    for post in posts: 
        USER_AGENTS = ['Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36']            
        response = requests.get(post,headers={'User-Agent': USER_AGENTS[0]})  
        response.raise_for_status() 
        soup = BeautifulSoup(response.text, 'html.parser')
        body = soup.find('body')
        script_tag = body.find('script')
        raw_string = script_tag.text.strip().replace('window._sharedData =', '').replace(';', '')
        metrics = json.loads(raw_string)
        for metric in metrics['entry_data']['PostPage'][0]['graphql']['shortcode_media']['edge_media_preview_comment']['edges']:
            try:                            
                comment = metric['node']['text']                                             
                to, comment = comment.split()[0].split('@')[1],str(' ').join(comment.split()[1:])
                frm = metric['node']['owner']['username']
                if not [to,frm] in t:
                    print([to,frm]) 
                    t.append([to,frm])  
            except Exception as e:
                continue    
    return t

def get_users(api, ids):
    """Obtiene los nombres de los usuarios que siguen a x   
    -param api: Sesion de Twitter API
    -param ids: lista de usuarios con la cual realizar la busqueda
    -returns:    
        -t: lista de usuarios en la red   
        -w: conteo de seguidores"""
    t = []
    ids = str().join(ids)
    try:                                                           
            for j in api.search(screen_name = ids,q='to:'+ids, count = 300, include_rts = True):
                t.append([j._json['user']['screen_name'],j._json['in_reply_to_screen_name']])
            t.append(ids)
    except tweepy.TweepError:                                      
            print ("El usuario " + str(ids) + " no admite recoleccion de sus tweets")
 
    return t      

def create_nodes(t, ids): 
    """Crear nodos en base a los usuarios que son amigos de x. La regla para la creacion de los nodos es que los nodos deben tener una conexion establecida  
    -param t: lista de usuarios que amigos de x
    -param ids: lista de usuarios con la cual realizar la busqueda
    -returns:    
        -nodes: lista de nodos en la red especificando el usuario x de origen"""                                                  
    links = []     #comenzamos la busqueda de vinculos entre los resultados (tiene que haber al menos un usuario que este siguiendo a los usuarios
    counter = 0                                                        
    for i in range(len(ids)):                                                      
        for j in range(len(t[i])):                                    
            if [t[i][j], counter] not in links:                        
                links.append([t[i][j], counter])                       
        counter += 1                                                   

    common_links = np.array(links)

    unqs = np.unique(common_links[:,0], return_counts = True, return_index = True) #a estos followers en comun los tenemos que ordenar en base a las personas que siguen

    nodes = [] #una vez que tomamos las personas comenzamos a formar los nodos
    for i in range(len(common_links)):
        for j in range(len(unqs[1])):
            if common_links[i][0] == common_links[unqs[1][j]][0]:
                nodes.append([common_links[i][0], common_links[i][1], common_links[unqs[1]][j][1]])

    imag_nodes = []
    for i in nodes:
        imag_nodes.append(np.unique(i))

    real_nodes = []
    for i in imag_nodes:
        if len(i) < 2:
            pass
        else:
            real_nodes.append(i) #hay nodos mientras hayan usuarios siguiendo mas de un usuario o mientras un usuario sea seguido por varios (+ de uno)

    nodes = []
    for i in real_nodes:
        try:                                                
            nodes.append((ids[int(i[0])], ids[int(i[1])], i[2]))
        except ValueError as e:
            continue

    return np.array(nodes)

def create_twitter_nodes(t):
    """Crear nodos en base a los usuarios que siguen a x. La regla para la creacion de los nodos es que los nodos deben tener una conexion establecida  
    -param t: lista de usuarios que siguen a x
    -param ids: lista de usuarios con la cual realizar la busqueda
    -returns:    
        -nodes: lista de nodos en la red especificando el usuario x de origen"""                                                     
    links = [] 
    for i in t:      
        try:                                                                
            if not None in i:    
                links.append(i)                                                           
        except Exception as e:
            pass 
    return np.array(links) 

def edges(nodes_list): 
    """Crear ejes en base a los datos de conexion de la lista de nodos 
    -param nodes_list: lista de nodos
    -returns:    
        -ejes: tupla conteniendo los ejes en la red"""                                                  
    edges = []                  
    for i in nodes_list:
        for j in list(permutations(i,2))[:len(i)]:
            if not (j[1], j[0]) in edges:
                edges.append(j)                                                                      
    return tuple(set(edges))

def generar_nodos(ejes): #tenemos que tomar los vertices como nodos para que la visualizacion sea la correcta, es decir, asumir que cada usuario es un nodo
    """Generar nodos en la red en base a los ejes
    -param ejes: tupla de ejes en la red
    -returns:    
        -nodos: tupla de nodos en la red"""  
    nodos = []  
    for eje in ejes:       
        if not eje[0] in nodos:
            nodos.append(eje[0])
        if not eje[1] in nodos:
            nodos.append(eje[1])
    return tuple(nodos)  

def grafico_dirigido(ejes, nodos):
    """Crear grafico dirigido para el analisis de la red social 
    -param ejes: tupla de ejes
    -param nodos: tupla de nodos
    -returns:    
        -G: grafico dirigido"""  
    G = nx.DiGraph() #iniciamos un grafico dirigido, es decir, donde la simetria entre todos los vertices de los nodos esta quebrada
    G.add_nodes_from(nodos) #agregamos los nodos (usuarios)
    G.add_edges_from(ejes) #agregamos los vinculos correspondientes, entendidos como ejes

    return G

def get_nombres_miembros(miembros, api): 
    """Obtener los nombres de aquellos usuarios cuyo nombre en la red esta marcado por una id de usuario en vez de una cuenta
    -param miembros: ejes de un grafico
    -param api: sesion de API
    -returns:    
        -members: lista de los ejes en la red con las ids reemplazadas con los nombres correspondientes"""      
    members = [[] for i in range(len(miembros))]
    for i in range(len(miembros)):
        try:
            members[i].append((miembros[i][0], api.get_user(miembros[i][1]).name))   
            print("En el vertice " + str(i) + " estan " + str(members[i])) #recuperamos los nombres de los usuarios
        except tweepy.error.TweepError:
            continue
    return members

Usage = """ python3 redes.py [-F/-T/-LI/-I]                     
        Opciones requeridas:
            -F: generar grafico de red en Facebook         
            -T: generar grafico de red en Twitter
            -LI: generar grafico de red en LinkedIn
            -I: generar grafico de red en Instagram
        Ejemplo:
            python3 redes.py -F"""

def main():
    if len(sys.argv) < 2:
        print ("\nBad amount of input arguments\n", Usage, "\n")
        sys.exit(1)
                                            
    if sys.argv[1] in ('-T'):
        api = login()
        user = sys.argv[2]
        users = list(user)                 
        t = get_users(api, users)
        t = create_twitter_nodes(t)
        ejes = t
        nodos = np.unique(t)
        print(ejes,nodos)

    if sys.argv[1] in ('-I'):
        posts = instagram_login(sys.argv[2])
        t = scrape_users(posts,sys.argv[2])
        ejes = t
        nodos = np.unique(t)

    if sys.argv[1] in ('-F'):
        session = login_facebook('mdevlopmentaccount@yahoo.com', '36727268ThePiper') 
        user = sys.argv[2]
        ejes = browse_friends(user,session)
        ejes = (tuple(i) for i in ejes)
        nodos = np.unique([list(i) for i in ejes])
        print(ejes,nodos)
        
    if sys.argv[1] in ('-LI'):
        obJH = Linkedin()
        nodes = obJH.start('oracle')                                    
        for i in nodes:
            ejes.append(['oracle',i])                        
        nodos = generar_nodos(ejes)                                  
    G = grafico_dirigido(ejes, nodos) #creamos un grafico dirigido y obtenemos una semilla
                                                                        
    lider = list(reversed(sorted(list(G.degree().__iter__()), key=lambda x: x[1])))[0][0] 
    if type(lider) != numpy.str_:
            try:                                                                   
                print ("Ego para analizar es: " + str(api.get_user(lider).screen_name))
            except Exception as e:                 
                print ("Ego para analizar es: " + str(lider))
    else:
            print("Ego para analizar es: " + lider)

    grafico_egos = nx.ego_graph(G, lider, center = True, undirected = True)

    coef = nx.average_clustering(nx.Graph(grafico_egos))

    print ("Coeficiente de Clustering en la red de egos: " + str(coef))

    if sys.argv[1] in ('-I'):
            csv_save('csv/egos_instagram.csv', list(grafico_egos.nodes().__iter__()))
            filename = 'csv/clustering_instagram.txt'
            write_gexf(grafico_egos, 'redes/egos_instagram.gefx')
            save_graph(grafico_egos, 'redes/egos_instagram.png')
    if sys.argv[1] in ('-T'):
            csv_save('csv/egos_twitter.csv', list(grafico_egos.nodes().__iter__()))
            filename = 'csv/clustering_twitter.txt'
            write_gexf(grafico_egos, 'redes/egos_twitter.gefx')
            save_graph(grafico_egos, 'redes/egos_twitter.png')
    if sys.argv[1] in ('-F'):
            csv_save('csv/egos_facebook.csv', G.nodes())
            filename = 'csv/clustering_facebook.txt'
            write_gexf(grafico_egos, 'redes/egos_facebook.gefx')
            save_graph(grafico_egos, 'redes/egos_facebook.png')
    if sys.argv[1] in ('-LI'):
            csv_save('csv/egos_linkedin.csv', grafico_egos.nodes())
            filename = 'csv/clustering_linkedin.txt'
            write_gexf(grafico_egos, 'redes/egos_linkedin.gefx')
            save_graph(grafico_egos, 'redes/egos_linkedin.png')

    with open(filename, 'w') as f:               
        f.write(str(coef))

if __name__ == '__main__': 
    main()

