from csv_data import *
from collections import Counter
import time
from yandex_translate import YandexTranslate
from nltk import FreqDist
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.pylab as lab
import pandas as pd
translate = YandexTranslate('trnsl.1.1.20170511T213958Z.f456c33d6b48dc86.b99c537f453f31be80efe1a9fe04e4bb944dd46c') #clave de Yandex Translate API

def plot_recommend(recommend, influencias, filename):
    fig, ax = plt.subplots()
    y = [float(count[1]) for count in recommend]
    x = range(1, len(y)+1)
    ax.set_yticklabels(y)
    ax.barh(x, y, align = 'center', tick_label = np.array(recommend))
    ax.set_title('Top Recommended', y = 1.15)
    lab.table(cellText = influencias, loc = 'bottom', colLabels = ['Usuario', 'Impacto', 'Red Social'])
    ax.xaxis.tick_top()
    fig.subplots_adjust(bottom = 0.4)
    fig.savefig(filename)

def main():
    msg = str()

    c_facebook = open('csv/clustering_facebook.txt').read()
    c_linkedin = open('csv/clustering_linkedin.txt').read()
    c_twitter = open('csv/clustering_twitter.txt').read()
    c_instagram = open('csv/clustering_instagram.txt').read()
    coeficientes = [c_twitter, c_facebook, c_linkedin,c_instagram]

    for i in range(len(coeficientes)):
        if i == 0:
            red = 'Twitter'
        elif i == 1:
            red = 'Facebook'
        else:
            red = 'LinkedIn'
        else:
            red = 'Instagram'
        if float(coeficientes[i]) < 0.3:
            msg = msg + ("\nLa red en " + red + " no exhibe conexiones fuertes y las relaciones entre los usuarios es muy pasiva; no hay intercambio de informacion sensible.\n")
        if float(coeficientes[i]) > 0.3 and float(coeficientes[i]) < 0.6:
            msg = msg + ("\nLa red en " + red + " exhibe unas pocas conexiones fuertes, por lo que puede haber variaciones en la informacion que se transmite.\n")
        if float(coeficientes[i]) > 0.6:
            msg = msg + ("\nLa red en " + red + " exhibe conexiones fuertes y relaciones entre los usuarios muy activas; aparece informacion que se intercambia entre todos y que se puede transformar en nueva informacion.\n")

    i_facebook = csv_read('csv/influencias facebook.csv', list)
    i_linkedin = csv_read('csv/influencias linkedin.csv', list)
    i_instagram = csv_read('csv/influencias instagram.csv', list)
    i_twitter = csv_read('csv/influencias twitter.csv', list) #empezamos contando las influencias totales a partir de los datos de twitter

    influencias = []
    for i in i_twitter: #juntamos todas las influencias
        i.append('twitter')
        influencias.append(i)
    for i in i_linkedin: #juntamos todas las influencias
        i.append('linkedin')
        influencias.append(i)
    for i in i_facebook:
        i.append('facebook')
        influencias.append(i)
    for i in i_instagram:
        i.append('instagram')
        influencias.append(i)

    influencias = list(reversed(sorted(influencias, key = lambda x: float(x[1]))))

    print ("La cantidad de influencias es " + str(len(influencias)) + ". Determine cual es la cantidad de usuarios influyentes que quiere obtener para el analisis. Escoger entre estos valores: [3,5,10,20,30,50,100]")

    n = int(input())

    influencias = influencias[:n]

    print ("Se ha generado un archivo .csv guardando los usuarios mas influyentes de todas las redes sociales")

    csv_save('influencias_redes_sociales.csv', influencias)

    p_facebook = csv_read('csv/palabras_facebook.csv', list)[1:]
    p_linkedin = csv_read('csv/palabras_linkedin.csv', list)[1:]
    p_twitter = csv_read('csv/palabras_twitter.csv', list)[1:]
    p_instagram = csv_read('csv/palabras_instagram.csv', list)[1:]

    palabras = []
    for i in p_facebook:
        palabras.append(i)
    for i in p_linkedin:
        palabras.append(i)
    for i in p_twitter:
        palabras.append(i)
    for i in p_instagram:
        palabras.append(i)

    for i in range(len(palabras)):                                    
        traduccion = translate.translate(palabras[i][0], 'en-es')['text'][0]                                          
        if traduccion != palabras[i][0]:     
            palabras[i][0] = traduccion 

    from collections import defaultdict
    result = defaultdict(list)
    for palabra,repeticiones in palabras:
        result[palabra].append(int(repeticiones))

    conteo_final = []      
    for i in result.keys():                     
        conteo_final.append((i, sum(result[i])))

    palabras = list(reversed(sorted(conteo_final, key = lambda x: x[1])))

    print ("La cantidad total de palabras mas utilizadas es " + str(len(palabras)) + ". Seleccione cuantas palabras desea utilizar para su estrategia")

    n = int(input())

    palabras = np.array(palabras[:n])[:,0].tolist()

    palabras = str(palabras).split('[')[1].split(']')[0]

    msg = msg + ("""\nA continuacion usted va a ver todas las palabras mas utilizadas por los usuarios en las redes sociales. Preste atencion a las palabras menos utilizadas y que puedan ser utiles para la estrategia: \n"""
            + '\n'+palabras+'\n')

    b_facebook = csv_read('csv/bigrama_facebook.csv', list)
    b_linkedin = csv_read('csv/bigrama_linkedin.csv', list)
    b_twitter = csv_read('csv/bigrama_twitter.csv', list)
    b_twitter = csv_read('csv/bigrama_instagram.csv', list)

    bigramas = [] 

    for i in b_twitter:                                    
        bigramas.append(' '.join(i))

    for i in b_facebook:                                   
        bigramas.append(' '.join(i))

    for i in b_linkedin:                                   
        bigramas.append(' '.join(i)) 

    for i in b_instagram:                                   
        bigramas.append(' '.join(i)) 

    print ("La cantidad total de bigramas computados es " + str(len(bigramas)) + ". Seleccione cuantas palabras desea utilizar para su estrategia")

    n = int(input())

    bigramas_mas_comunes = FreqDist(bigramas).most_common(n)

    msg = msg + ("""\nA continuacion usted va a ver los bigramas mas computados sobre la comunicacion en las redes sociales: \n""" + '\n'+str(bigramas_mas_comunes)+'\n')

    t_facebook = csv_read('csv/trigrama_facebook.csv', list)
    t_linkedin = csv_read('csv/trigrama_linkedin.csv', list)
    t_twitter = csv_read('csv/trigrama_twitter.csv', list)
    t_instagram = csv_read('csv/trigrama_instagram.csv', list)

    trigramas = [] 

    for i in t_twitter:                                    
        trigramas.append(' '.join(i))

    for i in t_facebook:                                   
        trigramas.append(' '.join(i))

    for i in t_linkedin:                                   
        trigramas.append(' '.join(i)) 

    for i in t_instagram:                                   
        trigramas.append(' '.join(i)) 

    print ("La cantidad total de trigramas computados es " + str(len(trigramas)) + ". Seleccione cuantas palabras desea utilizar para su estrategia")

    n = int(input())

    trigramas_mas_comunes = FreqDist(trigramas).most_common(n)

    msg = msg + ("""\nA continuacion usted va a ver los n-gramas mas computados sobre la comunicacion en las redes sociales: \n""" + '\n'+str(trigramas_mas_comunes)+'\n')

    n_facebook = csv_read('csv/n_grama_facebook.csv', list)
    n_linkedin = csv_read('csv/n_grama_linkedin.csv', list)
    n_twitter = csv_read('csv/n_grama_twitter.csv', list)
    n_instagram = csv_read('csv/n_grama_instagram.csv', list)

    ngramas = []
    for i in n_twitter:             
        for j in i:                       
            ngramas.append(j)
    for i in n_facebook:
        for j in i:                                   
            ngramas.append(j)
    for i in n_linkedin:     
        for j in i:                              
            ngramas.append(j)
    for i in n_instagram:     
        for j in i:                              
            ngramas.append(j)

    print ("La cantidad total de n-gramas computados es " + str(len(ngramas)) + ". Seleccione cuantas palabras desea utilizar para su estrategia")

    n = int(input())

    ngramas_mas_comunes = FreqDist(ngramas).most_common(n)

    msg = msg + ("""\nA continuacion usted va a ver los n-gramas computados sobre la comunicacion en las redes sociales: \n"""
            + '\n'+str(ngramas_mas_comunes)+'\n')

    s_facebook = csv_read('csv/sentimientos_facebook.csv', list)
    s_linkedin = csv_read('csv/sentimientos_linkedin.csv', list)
    s_instagram = csv_read('csv/sentimientos_instagram.csv', list)
    sentimientos = csv_read('csv/sentimientos_twitter.csv', list) #luego empezamos a juntar todos los textos en una sola lista

    for i in s_linkedin: #juntamos todas las influencias
        sentimientos.append(i)
    for i in s_facebook:
        sentimientos.append(i)
    for i in s_instagram:
        sentimientos.append(i)


    sents = []
    for i in sentimientos:
        sents.append(i[1])

    sentimientos = Counter(sents).most_common()

    if sentimientos[0][0] == 'neu':
        intencion = 'neutral. Esto indica que no hay muchas decisiones tomadas por los usuarios acerca de lo que estan diciendo.'
    elif sentimientos[0][0] == 'neg':
        intencion = 'negativa. Esto indica que las decisiones tomadas por los usuarios van en contra de lo que estan diciendo.'
    else:
        intencion = 'positiva. Esto indica que las decisiones tomadas por los usuarios van a favor de lo que estan diciendo.'
    msg = msg + ("\nEl sentimiento mas encontrado es " +intencion+'\n')

    r_facebook = csv_read('csv/recomendaciones_facebook.csv', list)[1:]
    r_linkedin = csv_read('csv/recomendaciones_linkedin.csv', list)[1:]
    r_twitter = csv_read('csv/recomendaciones_twitter.csv', list)[1:]
    r_instagram = csv_read('csv/recomendaciones_instagram.csv', list)[1:]

    plot_recommend(r_facebook, i_facebook, 'plots facebook.png')
    plot_recommend(r_twitter, i_twitter, 'plots twitter.png')
    plot_recommend(r_linkedin, i_linkedin, 'plots linkedin.png')
    plot_recommend(r_instagram, i_instagram, 'plots linkedin.png')

    msg = msg + ("\nContenido recomendable en Facebook:\n")

    for i in r_facebook:
        if float(i[1]) < 0.3:
            msg = msg + ("\nSe puede recomendar contenidos de " + i[0] + " sabiendo que es necesario utilizar vocabulario que mejore la comunicacion. Hay " + i[2] + " usuarios que pueden estar interesados en lo que " + i[0] + " recomienda\n")
        if float(i[1]) > 0.3 and float(i[1]) < 0.6: 
            msg = msg + ("\nSe puede recomendar contenidos de " + i[0] + " con mediana aceptacion. Hay " + i[2] + " usuarios que estan interesados\n")
        if float(i[1]) > 0.6:
            msg = msg + ("\nSe puede recomendar mucho contenido de o relacionado con " + i[0] + " contando con " + i[2] + " usuarios en la red que lo quieran\n")

    msg = msg + ("\nContenido recomendable en Twitter:\n")

    for i in r_twitter:
        if float(i[1]) < 0.3:
            msg = msg + ("\nSe puede recomendar contenidos de " + i[0] + " sabiendo que es necesario utilizar vocabulario que mejore la comunicacion. Hay " + i[2] + " usuarios que pueden estar interesados en lo que " + i[0] + " recomienda\n")
        if float(i[1]) > 0.3 and float(i[1]) < 0.6: 
            msg = msg + ("\nSe puede recomendar contenidos de " + i[0] + " con mediana aceptacion. Hay " + i[2] + " usuarios que estan interesados\n")
        if float(i[1]) > 0.6:
            msg = msg + ("\nSe puede recomendar mucho contenido de o relacionado con " + i[0] + " contando con " + i[2] + " usuarios en la red que lo quieran\n")

    msg = msg + ("\nContenido recomendable en Instagram:\n")

    for i in r_instagram:
        if float(i[1]) < 0.3:
            msg = msg + ("\nSe puede recomendar contenidos de " + i[0] + " sabiendo que es necesario utilizar vocabulario que mejore la comunicacion. Hay " + i[2] + " usuarios que pueden estar interesados en lo que " + i[0] + " recomienda\n")
        if float(i[1]) > 0.3 and float(i[1]) < 0.6: 
            msg = msg + ("\nSe puede recomendar contenidos de " + i[0] + " con mediana aceptacion. Hay " + i[2] + " usuarios que estan interesados\n")
        if float(i[1]) > 0.6:
            msg = msg + ("\nSe puede recomendar mucho contenido de o relacionado con " + i[0] + " contando con " + i[2] + " usuarios en la red que lo quieran\n")

    msg = msg + ("\nEmpresas e Industrias recomendables en Linkedin:\n")

    for i in r_linkedin:
        if float(i[1]) < 0.3:
            msg = msg + ('\n'+i[0] + " se puede llegar a recomendar si se mejora la comunicacion entre profesionales\n")
        if float(i[1]) > 0.3 and float(i[1]) < 0.6: 
            msg = msg + ('\n'+i[0] + " es recomendable con mediana aceptacion, es necesario mejorar un poco mas el contacto profesional\n")
        if float(i[1]) > 0.6:
            msg = msg + ('\n'+i[0] + " es altamente recomendable. Demuestra actitud correcta con sus conexiones\n")

    with open('resultados.txt', 'w') as f:                 
        f.write(msg)

    print ("Archivo .txt con los resultados del analisis generado satisfactoriamente")

if __name__ == '__main__':
    main()


